<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    public function engineer(){
    	return $this->belongsTo('App\Engineer');
    }

    public function citizen(){
    	return $this->belongsTo('App\Citizen');
    }
}
