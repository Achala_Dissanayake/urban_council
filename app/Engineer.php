<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engineer extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function comlpaints(){
    	return $this->hasMany('App\Complaint');
    }
}
