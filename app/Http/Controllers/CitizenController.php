<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Complaint;
use Auth;

class CitizenController extends Controller
{
    public function complainForm(){
    	return view('citizen/makecomplaint');
    }

    public function complain(Request $request){

        $this->validate($request, [
        	'title' => 'required|string',
        	'description' => 'required|string',
            'type' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        $complaint = new Complaint();
        $complaint->citizen_id=Auth::user()->citizen->id;
        $complaint->engineer_id= 0;
        $complaint->title=$request['title'];
        $complaint->description=$request['description'];
        $complaint->type=$request['type'];
        $complaint->status="not assigned";

        $now = date('Y-m-d') . '_' .date('H-i-s');
        $imageName = $complaint->citizen_id.'_'.$complaint->type.'_'.$now.'.'.$request->file('image')->getClientOriginalExtension();

        $path = base_path() .'/storage/app/citizens/'.$complaint->citizen_id .'_'.Auth::user()->name ;

        if(!is_dir($path)){
             mkdir($path,0777,true);
        }

        $request->file('image')->move($path , $imageName);

        $complaint->image_src= $path.'/'.$imageName;
        $complaint->image_name = $imageName;
        $complaint->save();

    	return redirect()->route('complaint', ['id' => $complaint->id]);
    }

    public function complaint($id) {

    	$complaint = Complaint::find($id);
    	return view('citizen/complaint')->with('complaint',$complaint);

    }

      public function getComplaintImage($id){
    	$complaint = Complaint::find($id);
    	$path = '/citizens/'.$complaint->citizen_id .'_'.Auth::user()->name.'/'.
    			$complaint->image_name ;
    	$image = Storage::disk('local')->get($path);
    	ob_end_clean();
        return response($image,200,['Content-type'=>'image/jpg']);
    }
}
