@extends('layouts.app')

@section('content')
@include('includes.message-block')
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
                <h1><b>Notices</b></h1>
            </div>
            <div class="col-md-3">
                <a href=""><h1>Latest Complaints</h1></a>
            </div>
        </div>
    </div>
    <div class="row">
        {{--Display General Posts--}}
        <div class="col-md-9" style="padding: 5%">
                <h2><b>Top Stories Today</b></h2>
                <hr style="border: 1px solid #ccc;">
                @if(Auth::user()->role == 'engineer')
                <?php $posts = App\Post::where('engineer', true)->where('deleted', false)->orderBy('id', 'DESC')->take(10)->get() ?>
                @elseif(Auth::user()->role == 'citizen')
                <?php $posts = App\Post::where('citizen', true)->where('deleted', false)->orderBy('id', 'DESC')->take(10)->get() ?>
                @else
                <?php $posts = App\Post::orderBy('id', 'DESC')->take(20)->get() ?>
                @endif
                @foreach($posts as $post)
                <article class="card">
                    <div>
                        <h2>
                            {{ $post['title'] }}
                        </h2>
                        <?php
                        $str = "$post->body";
                        echo htmlspecialchars_decode($str);
                        ?>
                    </div>

                    <div class="info" style="float:right;">
                        Posted on {{ $post['created_at'] }}
                    </div>
                    <br>
                    @if(Auth::user()->role == 'admin')
                    <div class="interaction" style="float:right;">
                        <a href="{{ route('view_update_post', ['id' => $post['id']]) }}" id="edit">Edit</a> |
                        <a href="{{ route('de_activate_post', ['id' => $post['id']]) }}">{{$post->deleted? 'Activate' : 'Deactivate'}}</a>
                    </div>
                    @endif
                </article>                
                <hr style="border: 1px solid #ccc;">
                <br>
                <br>
                @endforeach
        </div>
        <div class="col-md-3">
        @if(Auth::user()->role == 'admin')
         @foreach(App\Complaint::orderBy('created_at', 'desc')->take(10)->get() as $complaint)

                <div class="thumbnail" style="height: 400px;">
                    <div style="padding: 1%" align="center"> 
                        <img style="height: 200px;" alt="Gem Stone Pic" src="{{route('get_image',['id' => $complaint->id])}} " class="img-rounded img-responsive">
                    </div>
                    <div class="caption">
                        <h4 class="pull-right">Rs. {{$complaint->price}}.00</h4>
                        <h4><a href="#">{{$complaint->type->type}}</a><br><br>
                            {{$complaint->size->size}}
                        </h4>
                        <a href="{{route('shop_front',['id' => $complaint->shop_id])}}" >
                            <b>{{$complaint->shop->user->name}}</b><br>
                        </a>
                                              <p>{{$complaint->description}}</p>
                    </div>                            
                </div>
        @endforeach
        @else
             @foreach(App\Complaint::where('active',TRUE)->orderBy('created_at', 'desc')->take(10)->get() as $complaint)

                <div class="thumbnail" style="height: 400px;">
                    <div style="padding: 1%" align="center"> 
                        <img style="height: 200px;" alt="Gem Stone Pic" src="{{route('get_image',['id' => $complaint->id])}} " class="img-rounded img-responsive">
                    </div>
                    <div class="caption">
                        <h4 class="pull-right">Rs. {{$complaint->price}}.00</h4>
                        <h4><a href="#">{{$complaint->type->type}}</a><br><br>
                            {{$complaint->size->size}}
                        </h4>
                        <a href="{{route('shop_front',['id' => $complaint->shop_id])}}" >
                            <b>{{$complaint->shop->user->name}}</b><br>
                        </a>
                        <p>{{$complaint->description}}</p>
                    </div>                            
                </div>
        @endforeach
        @endif
    
        </div>
    </div>
</div>

@endsection
