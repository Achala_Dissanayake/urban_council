        <nav class="navbar navbar-inverse navbar-fixed-top" >
            <div  style="margin-right: 1%; margin-left:2%">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Gem Portal
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li> <a href="{{route('home')}}"> Home </a> </li>
                            <li> <a href="{{route('visit_shops')}}"> Complaints </a> </li>
                            <li> <a href="{{route('complain_form')}}"> Make Complaint </a> </li>

                            @if(Auth::user()->role == 'admin')
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Settings <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li> <a href="{{route('all_shops')}}"> User Settings </a> </li>
                                    <li> <a href="{{route('all_gems')}}"> Advertisement Settings</a></li>
                                    <li> <a href="{{route('all_posts')}}"> Post Settings</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li> <a href="{{route('admin_dash_board')}}"> Dash-Board</a></li>
                                </ul>

                            @endif

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">

                                    <li> <a href="{{route('profile_view')}}"> View Profile </a> </li>
                                    <li> <a href="{{route('update_view')}}"> Update Profile </a></li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li> <img src="#" style="align-content: center;"></li> -->
                        @endif
                    </ul>
                </div>
            </div>
        </nav>